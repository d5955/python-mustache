import chevron
import json

def create_env_file(env, data):
    with open("template.txt", "r") as i:
        template = i.read()

    args = {
      'template': template,

      'data': data
    }

    newFile = chevron.render(**args)
    with open(env + "_config_file.json", "w") as o:
        o.write(newFile)

with open("config_input.json") as i:
    config = i.read()

for env in json.loads(config)["environments"]:    
    for t in env:
        environment = t        
        data = {}
        data["environment"] = environment
        data["product"] = env[t]["product"]
        data["version"] = env[t]["version"]
        data["application"] = env[t]["application"]        
        create_env_file(environment, data)
