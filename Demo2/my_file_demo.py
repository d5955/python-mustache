import chevron

with open('my_infile.txt', 'r') as f:
    with open('my_changed_file.txt', 'w') as o:
        o.write(chevron.render(f, {'product': 'Chevron', 'person': 'Tony'}))
