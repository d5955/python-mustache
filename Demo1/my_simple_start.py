import chevron

my_output = chevron.render('Hello, {{ mustache }}!', {'mustache': 'World'})
print(my_output)
